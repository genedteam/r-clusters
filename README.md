# R Clusters #

This app uses R to perform a bag of words analysis of the GenEd course inventory with descriptions and/or syllabi. There are several dependencies managed in the script. 

### How do I get started? ### 

* Install R 
* Clone the repo
* Ensure the CSV is hosted at the location listed in the script (or download it from [https://bitbucket.org/genedteam/gened-course-data])
* Run the script

MIT License